CREATE DATABASE blog_db;

CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(300) NOT NULL,
  datetime_created DATETIME
  PRIMARY KEY (id)

);

CREATE TABLE posts (
	id INT 
	author_id INT,
	title VARCHAR(500),
	content VARCHAR(5000),
	datetime_posted DATETIME
	PRIMARY KEY (id)
	FOREIGN KEY  (author_id) REFERENCES users(id)
);

CREATE TABLE post_Comments(
	id INT,
	post_id INT,
	user_id INT,
	content VARCHAR(500),
	datetime_commented DATETIME,
	PRIMARY KEY(id),
	FOREIGN KEY(post_id) REFERENCES posts(id),
	FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE post_likes( 
	id INT, 
	post_id INT, 
	user_id INT, 
	datetime_liked DATETIME, 
	PRIMARY KEY(id), 
	FOREIGN KEY(post_id) REFERENCES posts(id), 
	FOREIGN KEY(user_id) REFERENCES users(id)
);

INSERT INTO Users(id, email, password, datetime_created) 
VALUES
	(0, "johnsmith@gmail.com","passwordA","2021-01-01 01:00:00"),
	(1, "juandelacruz@gmail.com","passwordB","2021-01-01 01:00:00"),
	(2, "janesmith@gmail.com","passwordC","2021-01-01 01:00:00"),
	(3, "mariadelacruz@gmail.com","passwordD","2021-01-01 01:00:00"),
	(4, "johndoe@gmail.com","passwordE","2021-01-01 01:00:00");

INSERT INTO posts(id, user_id, title, content, datetime_posted) 
VALUES
	(0, 1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
	(1, 1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
	(2, 2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
	(3, 4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

SELECT * FROM posts WHERE user_id = 1;

SELECT email, datetime_created FROM users;

UPDATE `posts` SET `content`="Hello to the people of the Earth!" WHERE id = 1; 

DELETE FROM `users` WHERE email="johndoe@gmail.com";
